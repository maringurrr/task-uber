//
//  TestViewController.swift
//  TaskUber
//
//  Created by mac-001 on 8/10/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit
import MapKit


private let screenWidth: CGFloat = UIScreen.main.bounds.width
private let screenHeight: CGFloat = UIScreen.main.bounds.height
private let scrollViewExpandedConstant: CGFloat = 80
private let scrollViewCollapsedConstant: CGFloat = UIScreen.main.bounds.size.height - 80
private let scrollViewAnimationHeight: CGFloat = scrollViewCollapsedConstant - scrollViewExpandedConstant
private let scrollViewsideMargin: CGFloat = 20
private let scrollViewExpandedFrame: CGRect = CGRect(x: scrollViewsideMargin, y: scrollViewExpandedConstant, width: screenWidth - 2 * scrollViewsideMargin, height: screenHeight - scrollViewExpandedConstant)
private let scrollViewCollapsedFrame: CGRect = CGRect(x: scrollViewsideMargin, y: scrollViewCollapsedConstant, width: screenWidth - 2 * scrollViewsideMargin, height: screenHeight - scrollViewCollapsedConstant)


class TestViewController: UIViewController {
  
  enum State {
    case collapsed
    case expanded
  }
  
  @IBOutlet private weak var mapView: MKMapView!
  @IBOutlet private weak var scrollView: UIScrollView!
  @IBOutlet private weak var scrollViewTopConstraint: NSLayoutConstraint!
  
  fileprivate var state: State = .collapsed { didSet { configureState() } }
  private var actualCompletion: CGFloat = 0
  private var previousPointY: CGFloat = 0
  private var panStartPointY: CGFloat = 0
  private var animationBaseCompletion: CGFloat = 0
  private var shouldCancel = false
  
  private lazy var panGesture = UIPanGestureRecognizer(target: self, action: #selector(handleTap(from:)))
  private lazy var animator: UIViewPropertyAnimator = self.goToUpAnimator()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    state = .collapsed
    addPanGesture()
  }
  
  fileprivate func addPanGesture() {
    panGesture = UIPanGestureRecognizer(target: self, action: #selector(handleTap(from:)))
    panGesture.delegate = self
    scrollView.addGestureRecognizer(panGesture)
  }
  
  private func goToUpAnimator() -> UIViewPropertyAnimator {
    let cubicParameters = UICubicTimingParameters(animationCurve: .linear)
    let animator = UIViewPropertyAnimator(duration: 1.0, timingParameters: cubicParameters)
    animator.isInterruptible = true
    
    animator.addAnimations {
      self.mapView.alpha = 0
      self.scrollView.frame = scrollViewExpandedFrame
      self.scrollView.contentOffset.y = 0
    }
    
    animator.addCompletion({ position in
      self.updateState()
    })
    
    animator.pauseAnimation()
    
    return animator
  }
  
  private func goToDownAnimator() -> UIViewPropertyAnimator {
    let cubicParameters = UICubicTimingParameters(animationCurve: .linear)
    let animator = UIViewPropertyAnimator(duration: 1.0, timingParameters: cubicParameters)
    animator.isInterruptible = true
    
    animator.addAnimations {
      self.mapView.alpha = 1
      self.scrollView.frame.origin = scrollViewCollapsedFrame.origin
      self.scrollView.bounds.size = scrollViewCollapsedFrame.size
    }
    
    animator.addCompletion({ position in
      self.updateState()
    })
    
    animator.pauseAnimation()
    
    return animator
  }
  
  private func configureState() {
    switch state {
    case .collapsed:
      scrollView.showsVerticalScrollIndicator = false
      scrollViewTopConstraint.constant = scrollViewCollapsedConstant
      
    case .expanded:
      scrollView.showsVerticalScrollIndicator = true
      scrollViewTopConstraint.constant = scrollViewExpandedConstant
    }
  }
  
  private func updateState() {
    state = scrollView.frame == scrollViewExpandedFrame ? .expanded : .collapsed
  }
  
  @objc private func handleTap(from recognizer: UITapGestureRecognizer) {
    let touchPoint = recognizer.location(in: view)
    let isCollapsed = state == .collapsed
    
    switch recognizer.state {
    case .began:
      panStartPointY = touchPoint.y
      previousPointY = touchPoint.y
      
      if animator.state == .inactive {
        // Previous animation finished. Must create new.
        if isCollapsed {
          animator = goToUpAnimator()
          animationBaseCompletion = 0
        } else {
          animator = goToDownAnimator()
          animationBaseCompletion = -scrollView.contentOffset.y / scrollViewAnimationHeight
        }
      } else {
        animator.pauseAnimation()
        animationBaseCompletion = animator.fractionComplete
      }
      
    case .changed:
      if isCollapsed {
        actualCompletion = animationBaseCompletion + (panStartPointY - touchPoint.y) / scrollViewAnimationHeight
        shouldCancel = previousPointY < touchPoint.y || actualCompletion < 0
        state = actualCompletion >= 1 ? .expanded : .collapsed
      } else {
        actualCompletion = animationBaseCompletion + (touchPoint.y - panStartPointY) / scrollViewAnimationHeight
        shouldCancel = previousPointY > touchPoint.y || actualCompletion < 0
        state = actualCompletion >= 1 ? .collapsed : .expanded
      }
      
      print(actualCompletion)
      
      animator.fractionComplete = min(1, max(0, actualCompletion))
      previousPointY = touchPoint.y
      
    case .ended, .cancelled:
      animator.isReversed = shouldCancel
      if actualCompletion > 0 && actualCompletion < 1 {
        animator.startAnimation()
      } else {
        animator.stopAnimation(true)
      }
      
    case .possible, .failed: break
    }
  }
}

extension TestViewController: UIScrollViewDelegate {
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    // Prevent scrolling in collapsed state
    if state == .collapsed {
      scrollView.contentOffset.y = 0
    }
  }
}

extension TestViewController: UIGestureRecognizerDelegate {
  func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    return true
  }
}
