//
//  TestViewController.swift
//  TaskUber
//
//  Created by mac-001 on 8/10/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit
import MapKit

class TestViewController: UIViewController {
  
  
  @IBOutlet weak var stackView: UIView!
  
  @IBOutlet weak var scrollView: UIScrollView!

  
  let upPositionViewHeight: CGFloat = 300
  let downPositionViewHeight: CGFloat = UIScreen.main.bounds.height - 80
  
  
  var longHightAnimation: CGFloat = 1
  
  lazy var downPositionView: CGRect = {
    let position = CGRect(x: 20, y: self.downPositionViewHeight, width: self.screenSize.width - 40, height: self.screenSize.height - self.downPositionViewHeight)
    return position
  } ()
  
  lazy var upPositionView: CGRect = {
    let position = CGRect(x: 0, y: self.upPositionViewHeight, width: self.screenSize.width, height: self.screenSize.height - self.upPositionViewHeight)
    return position
  } ()
  

  
  lazy var panGesture = UIPanGestureRecognizer(target: self, action: #selector(handleTap(from:)))
  
  var pointStartedPan: CGFloat = 50
  var pointStartedAnimation: CGFloat = 5000
  var pointFinishAnimation: CGFloat = 50
  
  var reversValue = false
  var oldPoint: CGFloat = 50
  var viewPositionUp = false
  var screenSize = UIScreen.main.bounds
  var animationStarted = false
  
  
  func makeAnimatorWithDuration(_ duration: TimeInterval) -> UIViewPropertyAnimator {
    let cubicParameters = UICubicTimingParameters(animationCurve:.linear)
    let animator = UIViewPropertyAnimator(duration: duration, timingParameters: cubicParameters)
    animator.isInterruptible = true
    return animator
  }
  
  lazy var animator: UIViewPropertyAnimator = {
    let cubicParameters = UICubicTimingParameters(animationCurve:.linear)
    let animator = UIViewPropertyAnimator(duration: 1.0, timingParameters: cubicParameters)
    animator.isInterruptible = true
    return animator
  }()
  lazy var animator2: UIViewPropertyAnimator = {
    let cubicParameters = UICubicTimingParameters(animationCurve:.linear)
    let animator = UIViewPropertyAnimator(duration: 1.0, timingParameters: cubicParameters)
    animator.isInterruptible = true
    return animator
  }()
  
  
  @IBOutlet weak var mapppView: MKMapView!

  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    
    scrollView.frame = downPositionView

    scrollView.delegate = self
    
    longHightAnimation = self.downPositionViewHeight - self.upPositionViewHeight

    addPanGesture()
  }
  
  fileprivate func addPanGesture() {
    
    panGesture = UIPanGestureRecognizer(target: self, action: #selector(handleTap(from:)))
    panGesture.delegate = self
    scrollView.addGestureRecognizer(panGesture)
    
  }
  
  private func addGoToUpAnimation(animator: UIViewPropertyAnimator) {
    
    animator.addAnimations {
      self.scrollView.frame = self.upPositionView
      
      self.mapppView.alpha = 0.0
    }
  }
  private func addGoToDownAnimation(animator: UIViewPropertyAnimator) {
    animator.addAnimations {
      self.scrollView.frame = self.downPositionView
      self.mapppView.alpha = 1.0
      
    }
  }
  

  
  func handleTap(from recognizer: UITapGestureRecognizer) {

    let touchPoint = recognizer.location(in: view)

    
    if viewPositionUp {
      
      switch recognizer.state {
        
      case .began:
        
        scrollView.contentSize.height = stackView.bounds.height
        
        
        
        pointStartedPan = touchPoint.y
                pointStartedAnimation = touchPoint.y + scrollView.contentOffset.y
        print(pointStartedAnimation)
        oldPoint = touchPoint.y
        
      case .changed:
        
        reversValue = oldPoint < touchPoint.y ? false : true

        if touchPoint.y > pointStartedAnimation && !animationStarted {
          scrollView.contentSize.height = screenSize.height - upPositionViewHeight
          if !reversValue {
            
            pointStartedAnimation = touchPoint.y
            
            
            animationStarted = true
            animator2 = makeAnimatorWithDuration(1)
            addGoToDownAnimation(animator: animator2)
            animator2.startAnimation()
            animator2.pauseAnimation()
            
          }
        }
        
        if animationStarted {
          
          switch touchPoint.y {
            
            
          case pointStartedAnimation..<screenSize.height:
            animator2.fractionComplete = (touchPoint.y - pointStartedAnimation) / longHightAnimation
 
          case 0..<pointStartedAnimation:
            
            if animator2.isRunning {
              animator2.stopAnimation(false)
              animationStarted = false
            }
          default:
            print("default")
            break
          }
          
        }
 
        oldPoint = touchPoint.y
        
      case .ended:
        
        animationStarted = false
        
        if reversValue {
          addGoToUpAnimation(animator: animator2)
          animator2.startAnimation()
          viewPositionUp = true
        } else {
          if touchPoint.y > pointStartedAnimation {
            addGoToDownAnimation(animator: animator2)
            animator2.startAnimation()
            viewPositionUp = false
          }
        
        }
      case .cancelled:
        print("canceled")
        
      default: break
      }

      
    } else {
      switch recognizer.state {
        
      case .began:
        
        
        scrollView.contentSize.height = screenSize.height - downPositionViewHeight
        pointStartedPan = touchPoint.y
        oldPoint = touchPoint.y
        
      case .changed:
        
        reversValue = oldPoint > touchPoint.y ? false : true
        if animationStarted {
          scrollView.contentSize = CGSize(width: scrollView.frame.size.width, height: screenSize.height - (touchPoint.y - (pointStartedPan - self.downPositionView.minY)))
          switch touchPoint.y {
          case pointStartedPan...3000:
            
            print("startAnimation")
            if animator.isRunning {
              animator.stopAnimation(false)
            }
            
          case (pointStartedPan - longHightAnimation)..<pointStartedPan:
            animator.fractionComplete = (pointStartedPan - touchPoint.y) / longHightAnimation

          default:
            break
          }
          
        } else {
          if !reversValue {
            animator = makeAnimatorWithDuration(1)
            addGoToUpAnimation(animator: animator)
            animator.startAnimation()
            animator.pauseAnimation()
            animationStarted = true
          }
        }

        oldPoint = touchPoint.y
        
      case .ended, .cancelled:
        
        animationStarted = false
        
        if reversValue {
          addGoToDownAnimation(animator: animator)
          animator.startAnimation()
          
          viewPositionUp = false
        } else {
          if touchPoint.y > (pointStartedPan - longHightAnimation) {
            addGoToUpAnimation(animator: animator)
            animator.startAnimation()
          }
          viewPositionUp = true
        }
        
      default: break
      }
    }
  }
  
}

extension TestViewController: UIScrollViewDelegate {
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
  }
}

extension TestViewController: UIGestureRecognizerDelegate {
  
  func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    return true
  }
  
}


